**Tested on FIREFOX 68 and with Dark Youtube**

*Firefox Code Injector: [Link](https://addons.mozilla.org/en-US/firefox/addon/codeinjector/)*








**Screenshots**
[![https://imgur.com/WhBuqjp.png](https://imgur.com/WhBuqjp.png)](https://imgur.com/WhBuqjp.png)
[![https://imgur.com/Y1zsic6.png](https://imgur.com/Y1zsic6.png)](https://imgur.com/Y1zsic6.png)



**Installation Instruction** 

**EN**

1. Download Code injector
2. Go to https://www.youtube.com
3. Click on Code Injector logo
4. Click Add Rule
[![https://imgur.com/1nzdfFk.png](https://imgur.com/1nzdfFkl.png)](https://imgur.com/1nzdfFkl.png)
5. Click Current Host 
6. In the JavaScript tab, paste `document.getElementById('logo-icon-container').innerHTML = "<img src=https://ci.phncdn.com/www-static/images/pornhub_logo_straight.png style=''>"`
7. In the CSS tab, paste the content of the *youtubeph.css* file
8. Save and refresh page





**PL**
1.  Pobierz code injectora   

2.  Wejdź na https://www.youtube.com
3.  Kliknij na logo code injectora
4.  Kliknij Add Rule
[![https://imgur.com/1nzdfFk.png](https://imgur.com/1nzdfFkl.png)](https://imgur.com/1nzdfFkl.png)
5.  Kliknij Current host
6.  W zakladce JavaScript wklej `document.getElementById('logo-icon-container').innerHTML = "<img src=https://ci.phncdn.com/www-static/images/pornhub_logo_straight.png style=''>"`
7.  W zakładce CSS wklej zawartość pliku *youtubeph.css*
8.  Zapisz i odswież stronę


